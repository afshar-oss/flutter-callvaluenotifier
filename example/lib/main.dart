import 'package:flutter/material.dart';
import 'package:callvaluenotifier/callvaluenotifier.dart';

void main() {
  runApp(MyApp());
}

Future<String> getMyValue() {
  return Future.delayed(
      Duration(seconds: 2), () => 'I am the future result value');
}

final myValue = CallValueNotifier<String>('I am the default value', getMyValue);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  int _counter = 0;

  void reset() {
    myValue.reset();
    myValue.fetch();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CallValueNotifier Demo'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'This text will update',
            ),
            ValueListenableBuilder<String>(
                valueListenable: myValue,
                builder: (BuildContext context, String value, Widget child) {
                  return Text(
                    '$value',
                    style: Theme.of(context).textTheme.headline4,
                  );
                }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: reset,
        tooltip: 'Reset',
        child: Icon(Icons.refresh),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
