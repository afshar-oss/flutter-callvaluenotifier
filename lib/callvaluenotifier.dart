/// Library to provide a [ValueNotifier] that updates itself from a given
/// callable.
library callvaluenotifier;

import 'package:flutter/material.dart';

/// [ValueNotifier] that updates its internal value from a given callable.
class CallValueNotifier<T> extends ValueNotifier<T> {
  final T originalValue;
  final Future<T> Function() valueCallable;

  /// Creates a new [CallValueNotifier] with a default [value] which is then
  /// updated once the callable returns.
  CallValueNotifier(T this.originalValue, Future<T> this.valueCallable())
      : super(originalValue) {
    fetch();
  }

  /// Resets the [ValueNotifier] to its original value.
  void reset() {
    value = originalValue;
  }

  /// Causes the value to be fetched.
  void fetch() {
    valueCallable().then((T v) => value = v);
  }
}
