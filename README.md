# callvaluenotifier

A Flutter package to provide a ValueNotifier that knows how to refresh its
value.

## Example

```dart
Future<String> getMyValue() {
  return Future.delayed(
      Duration(seconds: 2), () => 'I am the future result value');
}

final myValue = CallValueNotifier<String>('I am the default value', getMyValue);
```

then later

```dart
ValueListenableBuilder<String>(
    valueListenable: myValue,
    builder: (BuildContext context, String value, Widget child) {
      return Text(
        '$value',
        style: Theme.of(context).textTheme.headline4,
      );
    }),
```

Complete example in [the example directory](example/)
